package com.example.appdialogs

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.ScriptGroup
import android.util.DisplayMetrics
import android.view.Window
import android.view.WindowManager
import androidx.viewbinding.ViewBinding
import com.example.appdialogs.databinding.ActivityMainBinding
import com.example.appdialogs.databinding.DialogLayoutBinding
import extensions.setUp

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {

        binding = ActivityMainBinding.inflate(layoutInflater)

        super.onCreate(savedInstanceState)
        init()
        setContentView(binding.root)
    }
    private fun init() {
        binding.showDialog.setOnClickListener {
            showDialog()
        }
    }
    private fun showDialog() {
        val dialog = Dialog(this)
        val dialogBinding = DialogLayoutBinding.inflate(layoutInflater)
        dialog.setUp(android.R.color.transparent, WindowManager.LayoutParams.WRAP_CONTENT, dialogBinding)
        dialogBinding.btnClose.setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }
}