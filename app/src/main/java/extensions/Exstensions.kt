package extensions

import android.app.Dialog
import android.view.Window
import android.view.WindowManager
import androidx.viewbinding.ViewBinding


fun Dialog.setUp(color: Int, height: Int, binding:ViewBinding) {

    window!!.setBackgroundDrawableResource(android.R.color.transparent)
    window!!.requestFeature(Window.FEATURE_NO_TITLE)
    setContentView(binding.root)

    val params = window!!.attributes
    params.width = WindowManager.LayoutParams.WRAP_CONTENT
    params.height = height

}
